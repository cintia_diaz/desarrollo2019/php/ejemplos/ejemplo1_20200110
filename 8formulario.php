<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="8.php" method="get">
            <label for="inombre">Nombre del alumno</label>
            <input type="text" id="inombre" name="nombre">
            <label for="inumero1">Numero1</label>
            <input type="number" id="inumero1" name="numero[]">            
            <label for="inumero2">Numero2</label>
            <input type="number" id="inumero2" name="numero[]">
            
            <div>
                <label for="ipotes">Potes</label>
                <input id="ipotes" type="checkbox" name="poblacion[]" value="Potes" />
                <label for="isantander">Santander</label>
                <input id="isantander" type="checkbox" name="poblacion[]" value="Santander" />
            </div>
            <div>
                <label for="iRojo">Rojo</label>
                <input id="iRojo" type="radio" name="colores" value="R" />
                <label for="iAzul">Azul</label>
                <input id="iAzul" type="radio" name="colores" value="A" />
            </div>
            <div>
                <label for="inombres">Selecciona nombres</label>
                <select name="nombres[]" id="inombres" multiple>
                    <option value="0">Alberto</option>
                    <option value="1">Carmen</option>
                    <option value="2">Luis</option>                    
                </select>
            </div>
            <button>Enviar</button>
        </form>
        <?php
        
        ?>
    </body>
</html>
