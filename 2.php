<?php


// variables siempre con $ delante
// no son objetos
// no tienen propiedades
// son datos primitivos

$contador=0;
var_dump($contador);

$texto=null;
// concatenar en php
$texto.="a";
// $texto=$texto . "a";
// $texto=$texto+10;
var_dump($texto);

$cadena="";
var_dump($cadena);

$cadena=10;
var_dump($cadena);

$cadena="Ejemplo";
$longitud=strlen($cadena);
var_dump($longitud);