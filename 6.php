<?php

$numeros = [
    1,4,5,6,7,8,9
];

// imprimir los numeros del array
//for
for($c=0;$c<count($numeros);$c++){
?>

<!-- aquí empieza html -->
<!-- notacion corta sin echo -->
<li><?= $c .": " . $numeros[$c] ?></li>
 

<!-- otra opcion con echo -->
<?php
}
for($c=0;$c<count($numeros);$c++){
   
    echo "<li>" .$numeros[$c] ."</li>";
    // si tienes que meter clases tienes que escapar las comillas dobles o comenzar con comillas simples
    
    // con  comillas simples en php te escribe literal lo que ponga
    // si pones dobles, sustituye la variable por su valor
    
    echo "<li>$c:" . $numeros[$c] ."</li>";
    
    echo "<li>$c: $numeros[$c]</li>";
    
    
}
//while
$c=0;
 while($c<count($numeros)){
     echo "<li>$c: $numeros[$c]</li>";
     $c++;
 }

//foreach
// es como el forin de javascript
 
// $i es indice, $v es variable, indice es opcional
foreach ($numeros as $indice=>$valor){
    echo "<li>$indice: $valor</li>";
}
